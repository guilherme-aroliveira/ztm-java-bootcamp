package main.java._02_variables_arrays._08_double_variables;

public class Main {

    public static void main(String[] args) {

        double x = 10.4;
        double y = 3.8;
        double result = x / y;

        System.out.println("Quotient: " + result);
    }
}
