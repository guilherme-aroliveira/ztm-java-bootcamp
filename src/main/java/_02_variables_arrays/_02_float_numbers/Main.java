package main.java._02_variables_arrays._02_float_numbers;

public class Main {
    
    public static void main(String[] args) {
        
        float x = 4.5f;
        float y = 6.8f;

        float sum = x + y;

        System.out.println("x + y = " + sum);
    }
}
