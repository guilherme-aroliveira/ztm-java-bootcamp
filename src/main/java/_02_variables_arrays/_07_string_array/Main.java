package main.java._02_variables_arrays._07_string_array;

public class Main {

    public static void main(String[] args) {

        String[] fruits = {"guava", "peach", "blueberry", "banana", "apple"};

        System.out.print("Array elements: [");
        System.out.print(fruits[0] + ", ");
        System.out.print(fruits[1] + ", ");
        System.out.print(fruits[2] + ", ");
        System.out.print(fruits[3] + ", ");
        System.out.print(fruits[4]);
        System.out.println("]");

    }
}
