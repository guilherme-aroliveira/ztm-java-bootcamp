package main.java._02_variables_arrays._05_integer_values;

public class Main {

    public static void main(String[] args) {

        int a = 8;
        int b = 2;
        int result = a * b;

        System.out.println(a + " x " + b + " = " + result);

    }
}
