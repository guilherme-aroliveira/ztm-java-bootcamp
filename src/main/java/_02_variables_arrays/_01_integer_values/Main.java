package main.java._02_variables_arrays._01_integer_values;

public class Main {
    
    public static void main(String[] args) {
        
        int a, b;

        a = 7;
        b = 9;

        System.out.println("a: " + a);
        System.out.println("b: " + b);
    }
}
