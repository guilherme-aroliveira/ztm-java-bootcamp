package main.java._02_variables_arrays._04_integer_array;

public class Main {

    public static void main(String[] args) {

        int[] array = {1, 2, 3, 4, 5};

        System.out.print("\nArray elements: [");
        System.out.print(array[0] + ", ");
        System.out.print(array[1] + ", ");
        System.out.print(array[2] + ", ");
        System.out.print(array[3] + ", ");
        System.out.print(array[4]);
        System.out.print("]");

        System.out.println("\n");
        System.out.print("End");
    }
}
