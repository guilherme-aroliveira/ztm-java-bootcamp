package main.java._02_variables_arrays._03_two_strings;

public class Main {

    public static void main(String[] args) {

        String str1 = "Hello, ";
        String str2 = "World!";
        String concat = str1 + str2;

        System.out.println("Concatenated String" + concat);
    }
}
