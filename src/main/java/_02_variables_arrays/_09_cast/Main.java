package main.java._02_variables_arrays._09_cast;

public class Main {

    public static void main(String[] args) {

        double number = 10.6;
        int doubleNumber = (int) number;

        System.out.println("Casted nr: " + doubleNumber);
    }
}
