package main.java._02_variables_arrays._06_length_string;

public class Main {

    public static void main(String[] args) {

        String text;
        text = "Java Programming";

        int length = text.length();
        System.out.println("The length of the String \"" + text + "\" is: " + length);
    }
}
