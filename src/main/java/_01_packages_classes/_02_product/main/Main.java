package main.java._01_packages_classes._02_product.main;

import main.java._01_packages_classes._02_product.product.Inventory;
import main.java._01_packages_classes._02_product.product.Product;
import main.java._01_packages_classes._02_product.product.Warehouse;

public class Main {
    
    public static void main(String[] args) {
        
        Product product = new Product();
        product.productId = 3280;
        product.productName = "MacBook";
        product.price = 799.0;

        Inventory inventory = new Inventory();
        inventory.product = product;
        inventory.quantity = 10;

        Warehouse warehouse = new Warehouse();
        warehouse.warehouseId = 9895;
        warehouse.warehouseName = "Main warehouse";
        warehouse.inventories = "Eletronics";

        System.out.println("=====================");
        System.out.println("Product ID: " + inventory.product.productId + "\nName: " + inventory.product.productName + "\nPrice: $" + inventory.product.price);
        System.out.println("Quantity: " + inventory.quantity);
        System.out.println("=====================");
        System.out.println("Warehouse ID: " + warehouse.warehouseId + "\nName: " + warehouse.warehouseName + "\nInventory: " + warehouse.inventories);
        System.out.println("=====================");

        System.out.println("");
    }
}
