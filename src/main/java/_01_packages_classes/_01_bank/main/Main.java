package main.java._01_packages_classes._01_bank.main;

import main.java._01_packages_classes._01_bank.bank.BankAccount;

public class Main {
    
    public static void main(String[] args) {
        
        BankAccount ba = new BankAccount();

        ba.accountNumber = 10943;
        ba.accountHolder = "Guilherme";
        ba.balance = 20000.0;

        System.out.println("Account Number: " + ba.accountNumber + "\nAccount Holder: " + ba.accountHolder + "\nBalance: R$" + ba.balance);
    }
}
