package main.java._01_packages_classes._04_football.footballtournament;

public class Match {
    
    public Team team1;
    public Team team2;
    public int score1;
    public int score2;
    public Team winner;
}
