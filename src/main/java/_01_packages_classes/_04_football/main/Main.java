package main.java._01_packages_classes._04_football.main;

import main.java._01_packages_classes._04_football.footballtournament.Match;
import main.java._01_packages_classes._04_football.footballtournament.Player;
import main.java._01_packages_classes._04_football.footballtournament.Team;
import main.java._01_packages_classes._04_football.footballtournament.Tournament;


public class Main {
    
    public static void main(String[] args) {
        
        Team team = new Team();
        team.name = "Atlético Mineiro";
        team.players = "Hulk, Paulinho, Zaracho, Vargas, Arana, Alan, Everson, Mariano, Patrick";

        Player player = new Player();
        player.age = 37;
        player.name = "Hulk";
        player.position = "atacante";
        player.team = team;

        Match match = new Match();
        match.team1 = team;
        match.team2 = team;
        match.score1 = 3;
        match.score2 = 2;
        match.winner = team;

        Tournament tournament = new Tournament();
        tournament.name = "Mineiro";
        tournament.teams = "America, Atlético, Cruzeiro, Caldense, Uberlândia, Tupi";
        tournament.matches = "42";

        System.out.println("\n================================================");
        System.out.println("Team Name: " + team.name + "\nPlayers: " + team.players);
        System.out.println("================================================");
        System.out.println("Match: " + match.team1.name + " x Cruzeiro" + "\nScore 1: " +  match.score1 + "\nScore 2: " + match.score2 + "\nWinner: " + match.winner.name);
        System.out.println("================================================");
        System.out.println("Tournament Name: " + tournament.name + "\nTeams: " +  tournament.teams + "\nMatches: " + tournament.matches);

        System.out.println("");
    }
}
