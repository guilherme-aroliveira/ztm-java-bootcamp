package main.java._01_packages_classes._03_university.main;

import main.java._01_packages_classes._03_university.university.Course;
import main.java._01_packages_classes._03_university.university.Enrollment;
import main.java._01_packages_classes._03_university.university.Student;

public class Main {
    
    public static void main(String[] args) {
        
        Student student = new Student();
        student.studentId = 202308;
        student.name = "Guilherme";

        Course course = new Course();
        course.courseId = 8895;
        course.courseName = "Algorithms";

        Enrollment enrollment = new Enrollment();
        enrollment.student = student;
        enrollment.course = course;

        System.out.println("======================");
        System.out.println("Student ID: " + enrollment.student.studentId + "\nName: " + enrollment.student.name);
        System.out.println("======================");
        System.out.println("Course ID: " + enrollment.course.courseId + "\nCourse: " + enrollment.course.courseName);
        System.out.println("======================");
    }
}
